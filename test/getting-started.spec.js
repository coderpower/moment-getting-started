var expect = require('expect.js');
var moment = require('moment');
var format = require('../sources/getting-started.js');

suite('Date with the format in moment', function(){

    test('Should return a string', function(){
        var result = format();
        expect(typeof result).to.be('string');
    });

    test('Should return the right date', function(){
        var expected = moment('2016-04-03').format('LLLL');
        var result = format();
        expect(result).to.be(expected);
    });

});