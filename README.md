### Getting Started in moment.js

Easy introduction, return the date wih the format in moment

```javascript
 var date = '2016-04-03';
 var format = 'LLLL';
 var result = moment(date).format(format);
```

For more information, please reer to the documentation: http://momentjs.com/#format-dates.